import sys

class SentinelSciHubArgs:

    def read_from_csv(self, csv_file_path):
        import csv
#        with open(csv_file_path, 'rb') as f:
        with open(csv_file_path, 'rt') as f:
            reader = csv.reader(f, delimiter ='=')
            values = dict(reader)

        self.username = values.get('userID')
        self.password = values.get('password')
        self.area = values.get('Area')
        self.dateofacqusittion = (values.get('fromDate'), values.get('toDate'))
        self.platformName = values.get('platformName')
        self.productType = values.get('productType')
        self.outFolder = values.get('outFolder')
        self.relativeOrbitNumber = values.get('relativeOrbitNumber')
        if self.relativeOrbitNumber != None:
            try:
                self.relativeOrbitNumber = int(self.relativeOrbitNumber)
            except:
                print("ERROR!!!! relativeOrbitNumber should be integer.")
                sys.exit()
                
        if self.platformName == 'Sentinel-1':
            self.orbitDirection = values.get('orbitDirection')
            self.polarisationMode = values.get('polarisationMode')
            self.sensorOperationalMode = values.get('sensorOperationalMode')     
        elif self.platformName == 'Sentinel-2':
            self.cloudCoverPercentage = values.get('cloudCoverPercentage')

    def __init__(self, filter_CSV_location = None):
        self.area = None  
        self.dateofacqusittion = None
        self.outFolder = '.\\'
        if filter_CSV_location == None:
            print("Enter location of CSV filter file")
        else: 
            self.read_from_csv(filter_CSV_location)