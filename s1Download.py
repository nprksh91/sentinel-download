# -*- coding: utf-8 -*-
"""
Last Updated on 13 March 2019
@author: Nikhil
"""
     
from sentinelsat import SentinelAPI
import SentinelSciHubArgs 
import os
from shutil import copyfile
import sys
import pandas as pd


def makeList(conArgs):
    if conArgs.platformName != 'Sentinel-1':
        print("Filter mismatch. Filter file is for " + conArgs.platformName)
        return

    print("Filters for this query for Sentinel-1: ")
    print("...................................")
    print("Area in WKT: ", conArgs.area)
    print(" ")
    print("Date:", conArgs.dateofacqusittion)
    print("...................................")
    print(" ")
    
    api = SentinelAPI(user = conArgs.username, password = conArgs.password, api_url='https://scihub.copernicus.eu/apihub/')
    print("Conneted to SciHub. Querying SciHub for data...." )

    if conArgs.productType != None:
        products = api.query(conArgs.area, conArgs.dateofacqusittion, platformname = 'Sentinel-1', producttype = conArgs.productType); print("Done.")
        print("Filter by productType: " + conArgs.productType)
    else:
        products = api.query(conArgs.area, conArgs.dateofacqusittion, platformname = 'Sentinel-1'); print("Done.")
    
    # convert to Pandas DataFrame and subset
    products_df = api.to_dataframe(products)
    
    if conArgs.orbitDirection != None:
        products_df = products_df.loc[products_df['orbitdirection'] == conArgs.orbitDirection]
        print("Filter by orbitDirection: " + conArgs.orbitDirection)
    if conArgs.polarisationMode != None:
        products_df = products_df.loc[products_df['polarisationmode'] == conArgs.polarisationMode]
        print("Filter by polarisationMode: " + conArgs.polarisationMode)
    if conArgs.sensorOperationalMode != None:
        products_df = products_df.loc[products_df['sensoroperationalmode'] == conArgs.sensorOperationalMode]
        print("Filter by sensorOperationalMode: " + conArgs.sensorOperationalMode)
    if conArgs.relativeOrbitNumber != None:
        products_df = products_df.loc[products_df['relativeorbitnumber'] == conArgs.relativeOrbitNumber]
        print("Filter by relativeOrbitNumber: " + str(conArgs.relativeOrbitNumber))
    

    print("Total Files: " + str(len(products_df)))
    outFolder = conArgs.outFolder
    
    if not os.path.exists(outFolder):
        print(outFolder + " doesn't esist. Creating the directory.")
        os.makedirs(outFolder)
        
    copyfile(path_filter_csv, outFolder + '\S1_filter.txt')
    # products_df = products_df.drop(['gmlfootprint', 'footprint', 'summary', 'filename', 'format', 'identifier', 'instrumentshortname', 'instrumentname', 'processed'], axis=1)
    # products_df = products_df.drop(['link', 'link_alternative', 'link_icon'], axis=1)
    try:
        products_df.to_excel(outFolder + '\S1_queryResults.xlsx')
    except:
        print("An exception occurred while writting " + outFolder + "\S1_queryResults.xlsx")
        print("Possible Solution --> Try closing the file, if it is already open in another window!!")
        sys.exit()
    
 

def downloadFromList(conArgs):
    outFolder = conArgs.outFolder
    if os.path.isfile(outFolder + '\S1_queryResults.xlsx') == False:
        print("!!!!!!!!!!--ERROR--!!!!!!!!!!!")
        print(outFolder + "\S1_queryResults.xlsx not found!!!!")
        return
    
    pd_xl = pd.read_excel(outFolder + '\S1_queryResults.xlsx') 
    uuidList = pd_xl['uuid'].tolist()
    print("Number of files to download: " + str(len(uuidList)))
    
    api = SentinelAPI(user = conArgs.username, password = conArgs.password, api_url='https://scihub.copernicus.eu/apihub/'); print("Conneted to SciHub. Downloading for data...." )
    api.download_all(products = uuidList, directory_path = outFolder)
    





inputArgs = sys.argv

if len(inputArgs) > 1:
    if inputArgs[1] == 'list':
        download = False
    elif inputArgs[1] == 'download':
        download = True
    else:
        print("Argument missing --> 'list' or 'download'.")
        sys.exit()
    
    if len(inputArgs) < 3:
        print('Path(s) to filter file missing.')
        sys.exit()
    else:
        input_filter_list = inputArgs[2:]
else:
    print("No arguments passed!! Generating list from S1_ExapmleFilter.txt.")
    input_filter_list = ['.\S1_ExampleFilter.txt']
    download = False



for path_filter_csv in input_filter_list:
    print; print
    print("Working on -> " + path_filter_csv)
    
    if os.path.isfile(path_filter_csv) == False:
        print("!!!!!!!!!!--ERROR--!!!!!!!!!!!")
        print(path_filter_csv + "not found!!!!")
        continue
    
    conArgs = SentinelSciHubArgs.SentinelSciHubArgs(path_filter_csv)

    if download == False:
        makeList(conArgs)
    elif download == True:
        downloadFromList(conArgs)
    else:
        print("Something is wrong!!")
        
        
if download == False:
    print("S1_queryResults.xlsx saved to output folder(s).")
elif download == True:
    print("Download Complete.")   
else:
    print("Something is wrong!!" )   